FROM openjdk:14-alpine as build
WORKDIR /app
COPY . /app
RUN ls
RUN ./gradlew build -x test
FROM openjdk:14-alpine
COPY --from=build /app/build/libs/Dictionary-*-all.jar Dictionary.jar
EXPOSE 8080
CMD ["java", "-Dcom.sun.management.jmxremote", "-Xmx128m", "-jar", "Dictionary.jar"]
