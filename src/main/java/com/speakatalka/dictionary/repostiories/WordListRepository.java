package com.speakatalka.dictionary.repostiories;

import com.speakatalka.dictionary.entity.WordListEntity;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.PageableRepository;

@Repository
public interface WordListRepository extends PageableRepository<WordListEntity, Integer> {
}
