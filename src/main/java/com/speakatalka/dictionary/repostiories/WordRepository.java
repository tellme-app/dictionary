package com.speakatalka.dictionary.repostiories;

import com.speakatalka.dictionary.entity.word.WordEntity;
import io.micronaut.data.annotation.Query;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.PageableRepository;

import java.util.List;
import java.util.Optional;

@Repository
public interface WordRepository extends PageableRepository<WordEntity, Integer> {

    @Query(
            value = "select * from main.word where word like CONCAT('%', :searchWord, '%')",
            nativeQuery = true
    )
    List<WordEntity> searchWord(String searchWord);

    Optional<WordEntity> findTopByWord(String word);

    boolean existsByWord(String word);
}
