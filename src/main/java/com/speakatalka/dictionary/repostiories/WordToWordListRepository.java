package com.speakatalka.dictionary.repostiories;

import com.speakatalka.dictionary.entity.word.WordToWordListEntity;
import io.micronaut.data.annotation.Join;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.data.repository.PageableRepository;

import java.util.List;

@Repository
public interface WordToWordListRepository extends PageableRepository<WordToWordListEntity, WordToWordListEntity> {

    @Join(value = "wordEntity", type = Join.Type.FETCH)
    Page<WordToWordListEntity> findAllByWordListId(Integer wordListId, Pageable pageable);

    @Join(value = "wordEntity", type = Join.Type.FETCH)
    List<WordToWordListEntity> findAllByWordListId(Integer wordListId);


}
