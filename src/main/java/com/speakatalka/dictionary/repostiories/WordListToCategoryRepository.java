package com.speakatalka.dictionary.repostiories;

import com.speakatalka.dictionary.entity.category.WordListToCategoryEntity;
import io.micronaut.data.annotation.Join;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.data.repository.PageableRepository;

import java.util.List;

@Repository
public interface WordListToCategoryRepository extends PageableRepository<WordListToCategoryEntity, WordListToCategoryEntity> {

    @Join(value = "wordListEntity", type = Join.Type.FETCH)
    Page<WordListToCategoryEntity> findAllByCategoryId(Integer categoryId, Pageable pageable);
//    @Join(value = "wordListEntity", type = Join.Type.FETCH)
//    List<WordListToCategoryEntity> findAll();

}
