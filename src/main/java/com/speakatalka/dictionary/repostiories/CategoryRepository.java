package com.speakatalka.dictionary.repostiories;

import com.speakatalka.dictionary.entity.category.CategoryEntity;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.PageableRepository;

@Repository
public interface CategoryRepository extends PageableRepository<CategoryEntity, Integer> {
    boolean existsByCategoryName(String categoryName);
}
