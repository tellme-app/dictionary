package com.speakatalka.dictionary.model;

import io.micronaut.data.model.Page;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Category {

    private Integer categoryId;
    private String categoryName;
    private Page<WordList> wordLists;
}
