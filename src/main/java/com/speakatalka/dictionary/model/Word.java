package com.speakatalka.dictionary.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Word {

    private Integer wordId;
    private String word;
    private String transcription;
    private String translations;
    private List<Example> examples;

}
