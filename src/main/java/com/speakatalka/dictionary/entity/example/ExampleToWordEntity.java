package com.speakatalka.dictionary.entity.example;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import javax.persistence.IdClass;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@IdClass(ExampleToWordEntityKey.class)
public class ExampleToWordEntity {

    @Id
    private Integer exampleId;
    @Id
    private Integer wordId;
    private Integer priority;

}
