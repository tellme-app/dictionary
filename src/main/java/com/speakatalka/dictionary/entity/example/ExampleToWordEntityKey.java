package com.speakatalka.dictionary.entity.example;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.persistence.Id;
import java.io.Serializable;

@Embeddable
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ExampleToWordEntityKey implements Serializable {

    @Id
    private Integer exampleId;

    @Id
    private Integer wordId;
}
