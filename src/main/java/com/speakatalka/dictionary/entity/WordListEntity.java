package com.speakatalka.dictionary.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "word_list")
public class WordListEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer wordListId;
    private String name;
    private String description;

}
