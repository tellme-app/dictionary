package com.speakatalka.dictionary.entity.translation;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "translation_to_word")
@Entity
@IdClass(TranslationToWordEntityKey.class)
public class TranslationToWordEntity {

    @Id
    private Integer translationId;
    @Id
    private Integer wordId;
    private Integer priority;
}
