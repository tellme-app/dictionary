package com.speakatalka.dictionary.entity.translation;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.persistence.Id;
import java.io.Serializable;

@Embeddable
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TranslationToWordEntityKey implements Serializable {

    @Id
    private Integer translationId;

    @Id
    private Integer wordId;
}
