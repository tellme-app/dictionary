package com.speakatalka.dictionary.entity.category;

import com.speakatalka.dictionary.entity.WordListEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "word_list_to_category")
public class WordListToCategoryEntity implements Serializable {

    @Id
    private Integer categoryId;

    @Id
    @Column(name = "word_list_id")
    private Integer wordListId;

    @OneToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "word_list_id", updatable = false, insertable = false)
    private WordListEntity wordListEntity;
}
