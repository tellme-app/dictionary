package com.speakatalka.dictionary.entity.word;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "word_to_word_list")
public class WordToWordListEntity implements Serializable {

    @Id
    private Integer wordListId;
    @Id
    @Column(name = "word_id")
    private Integer wordId;

    @OneToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "word_id", updatable = false, insertable = false)
    private WordEntity wordEntity;

}
