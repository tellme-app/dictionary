package com.speakatalka.dictionary.controllers;

import com.speakatalka.dictionary.model.Category;
import com.speakatalka.dictionary.model.WordList;
import com.speakatalka.dictionary.repostiories.WordListToCategoryRepository;
import com.speakatalka.dictionary.service.CategoryService;
import com.speakatalka.dictionary.service.WordListService;
import com.speakatalka.dictionary.service.WordService;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;
import io.micronaut.http.annotation.QueryValue;
import lombok.RequiredArgsConstructor;

import javax.annotation.Nullable;
import java.util.List;

@Controller
@RequiredArgsConstructor
public class MainController {

    private final WordListService wordListService;
    private final WordService wordService;
    private final CategoryService categoryService;

    @Get
    public Iterable<WordList> getWordLists(Pageable pageable) {
        return wordListService.getWordLists(pageable);
    }

    @Get("/list/{listId}")
    public WordList getWordListAndWords(@PathVariable Integer listId, @Nullable @QueryValue Integer packSize){
        return wordListService.getWordListAndWords(listId, packSize);
    }

    @Get("/list/{listId}/words")
    public Page<String> getAllWordsFromWordList(@PathVariable Integer listId, Pageable pageable){
        return wordService.getWordsByListId(listId, pageable);
    }

    @Get("/word/search")
    public List<String> searchWord(@QueryValue String query) {
        return wordService.search(query);
    }

    @Get("/category")
    public Page<Category> getCategoriesPage(Pageable pageable) {
        return categoryService.getCategoriesPage(pageable);
    }

    @Get("/category/{categoryId}")
    public Category getCategoryWithWordLists(@PathVariable Integer categoryId, Pageable pageable) {
        return categoryService.getCategoryWithWordLists(categoryId, pageable);
    }

}


/*
* Список всех списков, pageable (done)
* Слова по списку
* Поиск слов
* Список всех категорий, pageable
* Списки по категориям, pageable
* */
