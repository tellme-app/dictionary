package com.speakatalka.dictionary;

import io.micronaut.runtime.Micronaut;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;

@OpenAPIDefinition(
        info = @Info(
                title = "Dict API",
                version = "0.0",
                description = "Описание API сервера для Dict",
                license = @License(name = "Apache 2.0", url = "https://speakatalka.com"),
                contact = @Contact(url = "https://speakatalka.com", name = "SpeakaTalka", email = "team@speakatalka.com")
        )
)
public class Application {

    public static void main(String[] args) {
        Micronaut.run(Application.class, args);
    }
}
