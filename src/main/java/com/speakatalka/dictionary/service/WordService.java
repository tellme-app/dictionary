package com.speakatalka.dictionary.service;


import com.speakatalka.dictionary.entity.word.WordEntity;
import com.speakatalka.dictionary.entity.word.WordToWordListEntity;
import com.speakatalka.dictionary.repostiories.WordRepository;
import com.speakatalka.dictionary.repostiories.WordToWordListRepository;
import io.micronaut.context.annotation.Property;
import io.micronaut.context.annotation.Value;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

@Singleton
@RequiredArgsConstructor
@Slf4j
public class WordService {

    private final WordToWordListRepository wordToWordListRepository;
    private final WordRepository wordRepository;
    @Property(name = "wordlist.standardSizeOfPack")
    private Integer standardSizeOfPack;

    public List<String> getRandomPackOfWordsFromWordList(Integer listId, Integer size) {
        var actualSize = size == null ? standardSizeOfPack : size;
        var wordsFromList = wordToWordListRepository.findAllByWordListId(listId).stream()
                .map(WordToWordListEntity::getWordEntity)
                .map(WordEntity::getWord)
                .collect(Collectors.toList());
        List<String> resultPack = new ArrayList<>();
        Random random = new Random();
        random.ints(0, wordsFromList.size()-1)
                .limit(actualSize)
                .forEach(t -> resultPack.add(wordsFromList.get(t)));
        return resultPack;
    }

    public Page<String> getWordsByListId(Integer listId, Pageable pageable) {
        return wordToWordListRepository.findAllByWordListId(listId, pageable).map(WordToWordListEntity::getWordEntity)
                .map(WordEntity::getWord);
    }

    public List<String> search(String searchWord) {
        return wordRepository.searchWord(searchWord).stream()
                .map(WordEntity::getWord)
                .collect(Collectors.toList());
    }

    public Optional<WordEntity> getWordId(String word) {
        return wordRepository.findTopByWord(word);
    }

    public void save(List<String> words) {
        var filteredWords = words.stream()
                .map(t -> t.trim().strip())
                .distinct()
                .map(t -> WordEntity.builder()
                        .word(t)
                        .build())
                .filter(t -> !wordRepository.existsByWord(t.getWord()))
                .collect(Collectors.toList());
//                .forEach(t -> {
//                    System.out.println(t.getWord());
//                    wordRepository.save(t);
//                });
//                .collect(Collectors.toList());
        wordRepository.saveAll(filteredWords);
    }
}
