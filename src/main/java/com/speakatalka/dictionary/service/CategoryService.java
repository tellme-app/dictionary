package com.speakatalka.dictionary.service;

import com.speakatalka.dictionary.entity.category.CategoryEntity;
import com.speakatalka.dictionary.model.Category;
import com.speakatalka.dictionary.model.WordList;
import com.speakatalka.dictionary.repostiories.CategoryRepository;
import com.speakatalka.dictionary.repostiories.WordListToCategoryRepository;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.exceptions.HttpStatusException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Singleton;

@Singleton
@RequiredArgsConstructor
@Slf4j
public class CategoryService {

    private final CategoryRepository categoryRepository;
    private final WordListToCategoryRepository wordListToCategoryRepository;

    public CategoryEntity create(CategoryEntity categoryEntity) {
        if (categoryRepository.existsByCategoryName(categoryEntity.getCategoryName())) {
            var errorMessage = String.format("Категория с именем %s уже существует", categoryEntity.getCategoryName());
            createBadRequestError(errorMessage);
        }
        if (categoryEntity.getCategoryId() != null) {
            createBadRequestError("Поле categoryId должно быть пустым");
        }
        return categoryRepository.save(categoryEntity);
    }

    public Page<Category> getCategoriesPage(Pageable pageable) {
        return categoryRepository.findAll(pageable).map(t -> Category.builder()
                .categoryId(t.getCategoryId())
                .categoryName(t.getCategoryName())
                .build()
        );
    }

    public Category getCategoryWithWordLists(Integer categoryId, Pageable pageable) {
        var categoryEntityOptional = categoryRepository.findById(categoryId);
        if (categoryEntityOptional.isEmpty()) {
            var errorMessage = String.format("Категория с categoryId %d не существует", categoryId);
            createBadRequestError(errorMessage);
        }
        var categoryEntity = categoryEntityOptional.get();
        var wordListPage = wordListToCategoryRepository.findAllByCategoryId(categoryEntity.getCategoryId(), pageable)
                .map(t -> WordList.builder()
                        .id(t.getWordListId())
                        .name(t.getWordListEntity().getName())
                        .description(t.getWordListEntity().getDescription())
                        .build()
                );
        return Category.builder()
                .categoryName(categoryEntity.getCategoryName())
                .categoryId(categoryEntity.getCategoryId())
                .wordLists(wordListPage)
                .build();
    }

    private void createBadRequestError(String errorMessage) {
        log.error(errorMessage);
        throw new HttpStatusException(HttpStatus.BAD_REQUEST, errorMessage);
    }
}
