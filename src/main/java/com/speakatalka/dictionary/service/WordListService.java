package com.speakatalka.dictionary.service;

import com.speakatalka.dictionary.entity.WordListEntity;
import com.speakatalka.dictionary.entity.word.WordToWordListEntity;
import com.speakatalka.dictionary.model.WordList;
import com.speakatalka.dictionary.repostiories.WordListRepository;
import com.speakatalka.dictionary.repostiories.WordToWordListRepository;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.exceptions.HttpStatusException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Singleton;

@Singleton
@RequiredArgsConstructor
@Slf4j
public class WordListService {

    private final WordListRepository wordListRepository;
    private final WordService wordService;
    private final WordToWordListRepository wordToWordListRepository;

    public Page<WordList> getWordLists(Pageable pageable) {
        return wordListRepository.findAll(pageable)
                .map(t -> WordList.builder().id(t.getWordListId()).description(t.getDescription()).name(t.getName()).build());
    }

    public WordList getWordListAndWords(Integer listId, Integer size) {

        var wordListEntity = getWordList(listId);
        var words = wordService.getRandomPackOfWordsFromWordList(listId, size);
        return WordList.builder().words(words).name(wordListEntity.getName()).description(wordListEntity.getDescription()).build();
    }

    public WordListEntity create(WordListEntity wordListEntity) {
        return wordListRepository.save(wordListEntity);
    }

    public void add(WordList wordList) {
        wordList.getWords().forEach(word -> {
            var wordId = wordService.getWordId(word);
            wordId.ifPresent(wordEntity -> wordToWordListRepository.save(WordToWordListEntity.builder().wordId(wordEntity.getWordId()).wordListId(wordList.getId()).build()));
        });
    }

    private WordListEntity getWordList(Integer listId) {
        return wordListRepository.findById(listId).orElseThrow(() -> {
            var errorMessage = String.format("Список с id = %d не найден", listId);
            log.error(errorMessage);
            return new HttpStatusException(HttpStatus.NOT_FOUND, errorMessage);
        });
    }
}
